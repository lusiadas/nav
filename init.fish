set -g nav_bookmarks "$path/bookmarks"
set -g nav_history "$path/history"
test -s $nav_history -a "$nav_resume" = true
and cd (tail -1 $nav_history)

function update_history -v PWD -d "Manage navigation history"
  set -l cwd (command realpath $PWD)
  command realpath ~ | string match -q $cwd
  and return 0
  echo $cwd >> $nav_history
  test (count (command cat $nav_history)) -le 100
  and return 0
  command cp -f $nav_history "$PREFIX"/tmp/nav_history
  command tail -100 "$PREFIX"/tmp/nav_history > $nav_history
  command rm "$PREFIX"/tmp/nav_history
end
