# nav

> *A plugin for [Oh My Fish](https://www.github.com/oh-my-fish/oh-my-fish).*

[![GPL License](https://img.shields.io/badge/license-GPL-blue.svg?longCache=true&style=flat-square)](/LICENSE)
[![Fish Shell Version](https://img.shields.io/badge/fish-v3.0.1-blue.svg?style=flat-square)](https://fishshell.com)
[![Oh My Fish Framework](https://img.shields.io/badge/Oh%20My%20Fish-Framework-blue.svg?style=flat-square)](https://www.github.com/oh-my-fish/oh-my-fish)

<br/>

## Summary

### [1. Description](#description)

### [2. Example Usage](#example-usage)

### [3. Options](#options)

### [4. Installation](#installation)

### [5. Configuration](#configuration)

### [6. Thanks](#thanks)

## Description

**Navigational Assistance with Velocity**. In brief, it finds a folder whose name matches search patterns and makes it the current working directory. Ambiguities prompt the user to choose a directory from a list ordered on the basis of folders that have been used most often and most recently.

## Example usage
[![asciicast](https://asciinema.org/a/BVTfmYKMmB8baVXngV2dmeNwD.png)](https://asciinema.org/a/BVTfmYKMmB8baVXngV2dmeNwD)

## Options

### Navigation
```
nav [pattern] ...
If no argument is provided, list bookmarks. Otherwise, look for a directory in the navigation history that matches the patterns given and go there.

nav -w/--where (abbreviation 'w') [pattern] ...
Go to a directory in the navigation history.

nav -t/--to (abbreviation 't') [pattern] ...
Go to a bookmarked directory.

nav -f/--foward (abbreviation 'f') [pattern] ...
Go to the closest child folder that matches passed patterns.

nav -b/--back (abbreviation b) [pattern] ...
Go to the closest parent folder that matches passed patterns.

MODIFIERS

nav [-t/-w/-f/-b] -p/--print [pattern] ...
Print destination instead.

nav [-t/-w/-f/-b] -c/--commander [pattern] ...
Open directory using mc, a.k.a. the Midnight Commander. A directory can be opened for each panel by dividing search patterns using ','.

```
### Bookmarking
```
nav -s/--save (abbreviation s) [name] [destination] ...
Bookmark directories. If only a name is provided, the current directory is bookmarked.

nav -r/--remove (abbreviation r) [bookmark/all] ...
Remove some, or all, saved bookmarks or bookmark folders.

nav -m/--move (abbreviation m) [source] ... [destination]
Move or rename bookmarks or bookmark folders.

nav -a/--autoremove (abbreviation a)
Remove bookmarks of folders that no longer exist. Bookmarks with destinations starting with "/media" will be ignored.

nav -l/--list (abbreviation l) [folder] ...
List the contents of the bookmarks folder or some inner folders.

nav -d/--backup [restore] [file]
Create, or restore from, a backup file containing all bookmarks. If no file is specified, it'll create, or look for, a backup file in the current folder.
```
### Misc
```
nav -x/--abbr
Add, or otherwise remove, abbreviationes for interactive use.

nav -y/--assume_yes

Automatically give confirmation to any question that might arise.

nav -h/--help
Display these instructions
```

## Install

```fish
omf install nav
```

### Dependencies

> If any of the above dependencies isn't installed, you'll be prompted to install it during nav's own installation

#### Required

```
curl feedback grep peco mlocate sed
```

#### Optional

- To display bookmark folders neatly, as in the preview above, nav uses [tree](http://mama.indstate.edu/users/ice/tree/). Otherwise, it'll just use `ls`.

- Also, to use the `--commander` option, [mc](https://midnight-commander.org/) (a.k.a Midnight Commander) needs to be installed.

## Configuration

### Abbreviations

Use the following command to setup recommended abbreviations.

```fish
nav --abbr
```

### Start sessions within the last visited folder

When `nav` loads along with the shell, it can set the latest directory in the navigation history as the current working directory. This behaviour helps to avoid bloating the navigation history with directories the user frequently opens as soon as the terminal is launched.

To activate this behaviour add the line `set -g nav_resume true` to your fish config file. Like so:

```
echo 'set -g nav_resume true' >> ~/.config/fish/config.fish
```

and to remove said behavior, use this following line:

```
sed -i '/nav_resume/d' ~/.config/fish/config.fish
```

## Thanks

This script was inspired by, and was based on, the work of theses nice fellows:

- [z](https://github.com/rupa/z), by rupa
- [bashmarks](https://github.com/huyng/bashmarks), by huyng
- [bookmark_dir](https://github.com/maku77/bookmark_dir), by maku77
- [bd](https://github.com/vigneshwaranr/bd), by vigneshwaranr

Also, it was written with the constant support of fish's [gitter channel](https://gitter.im/fish-shell/fish-shell).

**Thank you all for your contributions!**

---

Ⓐ Made in Anarchy. No wage slaves were economically coerced into the making of this work.
