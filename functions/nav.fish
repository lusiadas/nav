set -l cmd (command basename (status -f) | command cut -f 1 -d '.')
function $cmd -V cmd -d "Navigational Assistance with Velocity"

  # Load dependencies
  source (command dirname (status -f))/../instructions.fish
  source (command dirname (status -f))/../dependency.fish
  function "$cmd"_main -V cmd
    dependency -n $cmd -p percol tree grep mlocate
    or return 1

    # Parse flags
    if argparse -n $cmd -x (string join -- ' -x ' h,w,t,f,b,s,r,m,l,a,x,d p,c {p,c},{h,s,r,m,l,a,x,d} | string split ' ') 'h/help' 'w/where' 't/to' 'f/foward' 'b/back' 's/save' 'r/remove' 'm/move' 'l/list' 'a/autoremove' 'x/abbr' 'd/backup' 'p/print' 'c/commander' 'y/assume-yes' -- $argv 2>&1 | read err
      err $err
      reg "Use |$cmd -h| to see examples of valid syntaxes"
      return 1
    end

    # Set variables
    set -l flag (set --name | string match -r -- '(?<=_flag_)[^pc]$')
    set -l modifier (set --name | string match -r -- '(?<=_flag_)[pc]$')
    set --query _flag_y
    and set -l yes true

    # Check for bookmark availability
    if string match -qr '[trmla]' $flag
      if not find $nav_bookmarks -type l 2>/dev/null | string length -q
        err "$cmd: No bookmarks were saved. Save a bookmark using |$cmd -s|:"
        "$cmd"_instructions "$cmd -s/--save"
        return 1
      end
    end

    # Check for argument
    set -l args $argv
    if not isatty
      while read -l line
        set --append args $line
      end
    end
    if string match -qr '[wtfsrm]' $flag
      if test -z "$args"
        "$cmd"_instructions "$cmd -$flag/--\S+"
        return 1
      end
    else if string match -qr '[ax]' $flag
      if test -n "$args"
        "$cmd"_instructions "$cmd -$flag/--\S+"
        return 1
      end
    end

    # Load options
    function "$cmd"_navigate -V args -V cmd -V yes -a flag modifier \
    -d 'Find and navigate to a directory'

      # Check if commander option was invoked
      if string match -q c $modifier
        if not type -qf mc
          if test -z "$yes"
            wrn "The installation of |mc|, a.k.a. midnight commander, is necessary to use the |--commander| option"
            read -n 1 -P "Install it? [y/n]:" | string match -qi y
            or return 1
          end
          dependency mc
          or return 1
        end
        if test (string match -ar -- , $argv | wc -w) -gt 1
          err "$cmd: More than 2 sequences of patterns were passed"
          "$cmd"_instructions "$cmd -c/--commander"
          return 1
        end
        set args (string split -- , "$args")
      end

      # Set pool
      set -l matches; set -l not_found

      # Back option: search parent folders
      if string match -q b $flag
        if string match -q b $argv
          set matches (dirname (realpath $PWD))
        else
          for i in (seq (count $args))
            string match -qr -- '/$' $args[$i]
            and set --append matches (command realpath $PWD \
            | string match -r -- "(?i).*$args[$i]")
            or set --append matches (command dirname (realpath $PWD) \
            | string match -r -- "(?i).*$args[$i][^/]*")
            test -n "$matches[$i]"
            or set --append not_found $args[$i]
          end
        end
      else
        for i in (seq (count $args))
          set -l pool

          # To option: search for bookmarks
          if string match -q t $flag
            if test -L "$nav_bookmarks/$args[$i]"
              set pool "$args[$i]"
            else
              set pool (find $nav_bookmarks -type l 2>/dev/null \
              | string match -r -- "(?<=$nav_bookmarks/).+")
              for pattern in (string split -- ' ' $args[$i])
                set pool (string match -er "(?i)$args" $pool)
              end
            end

          # Where option: search for paths
          else
            if string match -q w $flag
              if test -d "$args[$i]"
                set pool "$args[$i]"
              else
                set -l initial_query (string match -r '^\S+' "$args[$i]")
                if string match -qr -- / $args[$i]
                  set initial_query (command locate -ib "$args[$i]" \
                  | string match -ve .git)
                else
                  set initial_query (command locate -i "$args[$i]" \
                  | string match -ve .git)
                end
                for filter in (string split ' ' "$args[$i]")
                  set initial_query (string match -aei "$filter" $initial_query)
                end
                for result in $initial_query
                  test -d "$result"
                  and set --append pool $result
                end
              end

            # Foward option: Search child directories
            else
              if not find . -type d -maxdepth 1 2>/dev/null \
              | string length -q
                err "$cmd: No folders were found within this directory"
                return 1
              end
              set -l initial_query (locate $PWD \
              | string match -r "(?<=$PWD/).*" | string match -ve .git)
              if test -z "$initial_query"
                set --append not_found $args[$i]
                continue
              end
              set -l pool
              for result in $initial_query
                test -d "$result"
                or continue
                set --append pool $result
              end
              if string match -qr -- './$' "$args[$i]"
                set args[$i] (string match -r -- '.+(?=/$)' $args[$i])
                set pool (string match -r -- "$args[$i](?=\$)" $pool \
                | sort -u)
              else if string match -q -- / "$args[$i]"
                set pool (string match -r -- "^.+(?=/)" $pool \
                | sort -u)
              else
                set pool (string match -r -- "(?i).*$args[$i][^/]*" $pool \
                | sort -u)
              end
            end
          end

          # If no matches where found for the current pattern, remember that.
          if test -z "$pool"
            set --append not_found $args[$i]
            continue

          # If various matches were found, list them in order of relevance
          else if test (count $pool) -gt 1
            set -l relevance
            for match in $pool
              set -l priority
              if string match -q t $flag
                grep -n (realpath "$nav_bookmarks/$match") $nav_history \
                | string match -r '^[0-9]+' | string join + | bc \
                | read priority
              else
                grep -n $match $nav_history | string match -ar '^[0-9]+' \
                | string join + | bc | read priority
              end
              or set priority 0
              set --append relevance "$priority $match"
            end

            # And prompt the user to choose between the remaining matches
            printf "%s\n" $relevance \
            | sort \
            | string replace -ar "[0-9]+\s+" "" \
            | command percol --query $args[$i] \
            | read pool
            or return 1
          end
          string match -q f $flag
          and set --append matches $PWD/$pool
          or set --append matches $pool
        end
      end

      # If patterns weren't matched
      if test -n "$not_found"
        if string match -qv t $flag
          err "$cmd: $not_found: No matches were found"
          return 1
        end
        wrn "$not_found: No bookmarks found for pattern"
        reg "Searching for pattern in navigation history..."
        $cmd --where -$modifier $args

      # If args were matched
      else if string match -qr [tw] $flag
        # In the case of 'to' option, test the bookmark validity
        for i in (seq (count $matches) | sort -r)
          if string match -q t $flag
            if test -d "$nav_bookmarks/$matches[$i]"
              set matches[$i] "$nav_bookmarks/$matches[$i]"
              continue
            end
            set --query yes
            or set -l yes
            if test -z "$yes"
              wrn "The destination for bookmark |$matches[$i]| is unavailable"
              read -n 1 -P "Delete bookmark? [y/n]: " yes
            end
            string match -qr -- '(?i)(y|true)' $yes
            or continue
            sed -i "\|^"(realpath "$nav_bookmarks/$matches[$i]")"\$|d" \
            "$nav_history"
            rm "$nav_bookmarks/$matches[$i]"

          # In the case of 'where' option, test the historic validity
          else
            test -d "$matches[$i]"
            and continue
            sed -i "\|^$matches[$i]\$|d" $nav_history
          end
          set --erase matches[$i]
        end
      end

      # Navigate
      test "$matches"
      or return 1
      if string match -q c $modifier
        command mc $matches
      else if string match -q p $modifier
        echo $matches
      else
        string match -q $PWD $matches
        and dim "You're already there."
        or cd $matches 2>/dev/null
      end
    end

    function "$cmd"_bookmark -a flag -V args -V cmd -V yes \
    -d 'Manage, list, or backup directory bookmarks'

      # Load functions
      function "$cmd"_manage -a flag -V args -V cmd -V yes

        function "$cmd"_save -V args -V cmd -V yes \
        -d 'Bookmark directories'
          # Substitute relative paths for absolute paths
          for i in (seq 1 2 (count $args))
            string length -q $args[(math $i + 1)]
            and set args[(math $i + 1)] (realpath $args[(math $i + 1)])
          end

          # Substitute no path for the current path
          math (count $args) / 2 | string match -qe .
          and set --append args (realpath $PWD)

          for i in (seq 1 2 (count $args) | sort -r)
            set -l j (math $i + 1)
            set -l failed

            # Check if the bookmark name is valid
            if string match -r '^(all|history)$' $args[$i]
              err "$cmd: $args[$i]: Invalid bookmark name"
              set failed true
            end
            if string match -qr -- '(^|/)\.' $args[$i]
              err "$cmd: $args[$i]: Can't save bookmarks by naming them as hidden files or folders"
              set failed true
            end
            if string match -qr -- '\s' $args[$i]
              err "$cmd: $args[$i]: Can't save bookmarks with names containing whitespaces"
              set failed true
            end
            if string match -a $args[$i] $args[(seq 1 2 (count $args))] \
            | uniq -d | string length -q
              err "$cmd: $args[$i]: Can't save multiple bookmarks with the name"
              set failed true
            end

            # Check if bookmark path is valid
            if string match -a (realpath $args[$j]) \
              $args[(seq 2 2 (count $args))] | uniq -d | string length -q
                err "$cmd: $args[$j]: Can't save multiple bookmarks for the same path"
                set failed true
            end
            if not test (string sub -l 7 $args[$j]) = '/media/' \
            -o -d $args[$j]
              err "$cmd: $args[$j]: Directory not found"
              set failed true
            end
            if test "$failed"
              set -e args[$i $j]
              continue
            end

            # Check for a naming conflict
            if find $nav_bookmarks 2>/dev/null \
            | string match -r -- "(?<=$nav_bookmarks/)$args[$i](?=/|\$)" \
            | read same_name
              test -L "$nav_bookmarks/$same_name"
              and wrn "A bookmark named |$args[$i]| already exists."
              or wrn "A folder named |$args[$i]| already exists."
              if test -z "$yes"
                if read -n 1 -P "Overwrite it? [y/n]: " | string match -qvi y
                  dim "Skipped adding bookmark |$args[$i]|."
                  continue
                end
              end
              rm -r "$nav_bookmarks/$same_name"
            end

            # Check for matching destinations
            set -l bookmarks (find $nav_bookmarks -type l 2>/dev/null)
            if contains -i $argv[$j] (readlink $bookmarks 2>/dev/null) 2>/dev/null \
            | read k
              if test -z "$q"
                wrn "Directory |$path| already assigned to bookmark |"(command basename $bookmarks[$k])"|."
                if read -P "Replace it? [y/n]: " | string match -qvi y
                  dim "Skipped adding bookmark |$args[$i]|."
                  continue
                end
              end
              rm "$bookmarks[$i]"
            end

            # Create folders as needed, and save the bookmark
            mkdir -p (string match -r ".+/" $nav_bookmarks/$args[$i])
            ln -sf "$args[$j]" "$nav_bookmarks/$args[$i]"
            set --append saved $args[$i]
          end

          # Reply with result
          test "$saved"
          or return 1
          test (count $saved) -eq 1
          and win "Bookmark |$saved| added."
          or win "Bookmarks |"(string join "|, |" $saved)"| added."
        end

        function "$cmd"_remove -V args -V cmd -V yes \
        -d 'Erase bookmarks or navigation history'

          # Delete the navigation history, if such is the command
          if contains history $args
            if test -z "$yes"
              if read -p 'wrn "Delete navigation history? [y/n]: "' \
              | string match -qi y
                rm -r $nav_history
                win "History deleted"
              end
            end
            set --erase args[(contains -i history $args)]
            test "$args"
            or return 0
          end

          # Delete the bookmarks folder, if such is the command
          if contains all $args
            if test -z "$yes"
              read -n 1 -p 'wrn "Delete all bookmarks? [y/n]: "' \
              | string match -qi y
              or return 1
            end
            rm -fr "$nav_bookmarks"
            win "Bookmarks deleted"
            return 0
          end

          # Delete bookmarks or folders
          set -l bookmarks; set -l folders; set -l not_found
          for arg in $args
            if test -L "$nav_bookmarks/$arg"
              set bookmarks $bookmarks $arg
              rm "$nav_bookmarks/$arg"
            else if test -d "$nav_bookmarks/$arg"
              set folders $folders $arg
              rm -r "$nav_bookmarks/$arg"
            else
              err "$cmd: Neighter bookmarks nor folders were found for \"|$not_found|\""
            end
          end

          # Reply if bookmarks or folders were deleted.
          if test -z "$folders" -a -z "$bookmarks"
            reg "Use |$cmd -l| to see available bookmarks"
            return 1
          end
          if test -n "$bookmarks"
            test (count $bookmarks) -gt 1
            and set bookmarks 's |'(string join '|, |' $bookmarks)'|,'
            or set bookmarks " |$bookmarks|"
          end
          if test -n "$folders"
            test (count $folders) -gt 1
            and set folders 's |'(string join '|, |' $folders)'|,'
            or set folders " |$folders|"
          end
          if test -z "$folders"
            win "Bookmark$bookmarks removed."
          else if test -z "$bookmarks"
            win "Folder$folders removed."
          else
            win "Bookmark$bookmarks and folder$folders removed."
          end
        end

        function "$cmd"_move -V args -V cmd -V yes \
        -d 'Move or rename bookmarks or bookmark folders'

          # Check destination validity
          if string match -qr -- '^(all|history)$' $args[-1]
            err "$cmd: $args[-1]: Invalid bookmark or folder name"
            return 1
          end
          if string match -qr -- '(^|/)\.' $args[-1]
            err "$cmd: $args[-1]: Can not move bookmarks or folders by naming them as being hidden"
            return 1
          end
          if string match -qr -- '\s' $args[-1]
            err "$cmd: $args[-1]: Can not save bookmarks with names containing whitespaces"
            return 1
          end
          if contains $args[-1] $args[1..-2]
            err "$cmd: $args[-1]: Can not move |\$args[-1]| into itself"
            return 1
          end

          # Check if bookmarks or folders to be moved do exist
          set -l not_found
          for arg in $args[1..-2]
            test -e "$nav_bookmarks/$arg"
            or set --append not_found $arg
          end
          if test -n "$not_found"
            set not_found (string join '|, |' $not_found)
            err "$cmd: $not_found: No bookmarks nor folders where found for this pattern"
            reg "Use |$cmd -l| to see available bookmarks"
            return 1
          end

          # Check if the destination contains a preexisting bookmark
          if test -L $nav_bookmarks/$args[-1]
            if test -z "$yes"
              wrn "A bookmark |\$nav_bookmarks/$args[-1]| already exists."
              read -n 1 -P "Overwrite it? [y/n]: " | string match -qi y
              or return 1
            end
            rm "$nav_bookmarks/$args[-1]"
          end

          # Move bookmarks and folders
          mkdir -p (string match -r ".+/" $nav_bookmarks/$args[-1])
          mv "$nav_bookmarks/"{(string join , $args)}
          win "Moved |"(string join '|, |' $args[1..-2])"|, to |$args[-1]|."
        end

        function "$cmd"_autoremove -V args -V cmd \
        -d 'Autoremove symlinks to unexisting folders'

          # Check for invalid bookmarks
          for bookmark in $bookmarks
            test (readlink $bookmark | string sub -l 7) = '/media/' -o -d $bookmark
            and set --erase bookmarks[(contains -i $bookmark $bookmarks)]
          end

          # Autoremove bookmarks
          if test -z "$bookmarks"
            win "All bookmarks are valid"
            return 0
          end
          rm $bookmarks
          set bookmarks (string match -r "(?<=$nav_bookmarks/).+" $bookmarks)
          test (count $bookmarks) -eq 1
          and set bookmarks " |$bookmarks|"
          or set bookmarks "s |"(string join '|, |' $bookmarks)"|,"
          win "Bookmark$bookmarks removed."
        end

        # Call requested option
        switch $flag
          case s
            "$cmd"_save
          case r
            "$cmd"_remove
          case m
            "$cmd"_move
          case a
            "$cmd"_autoremove
        end

        # Erase emptied folders in the bookmark directory
        for folder in (find $nav_bookmarks -type d 2>/dev/null)
          ls $folder | string length -q
          or rm -fr $folder
        end
      end

      function "$cmd"_list -V args -V cmd

        # If arguments are present, check their validity
        set -l found
        for arg in $args
          test -d $nav_bookmarks/$arg
          and set --append found $nav_bookmarks/$arg
          or err "$cmd: $not_found: Bookmark folder not found"
        end

        # Print contents of listed folders
        test -z "$found" -a -z "$args"
        and set --append found $nav_bookmarks
        if test -z "$found"
          return 1
        else if test (count $found) -gt 1
          if type -qf tree
            for folder in $found
              echo (set_color --bold blue)(command basename $folder)$reg
              command tree -C $folder | tail +2 | head -n -1
            end
          else
            for folder in $found
              ls -l --color=always $bookmarks
              echo
            end
          end
        else
          if type -qf tree
            command tree -C $found | tail +2 | head -n -1
          else
            ls -l --color=always $bookmarks
            echo
          end
        end
      end

      function "$cmd"_backup -V args -V cmd -V yes

        # Restoring a bookmarks folder from a file
        if string match -q "restore" $args[1]
          if test -z "$args[2]"
            set -l backup (ls | string match -ar '.+\.bak$')
            if test -z "$backup"
              err "$cmd: No target backup file was specified, nor could be found in the current directory"
              return 1
            end
            if test (count $backup) -gt 1
              wrn "More than one backup file found in the current folder"
              for i in (seq (count $backup))
                reg "$i. $backup[$i]"
              end
              reg (math (count $backup) + 1)". all"
              read -P "Use which? [number/[c]ancel]: " opt
              test "$opt" -le (math (count $backup) + 1) 2>/dev/null
              or return 1
              test "$opt" -le (count $backup)
              and set --append args $backup[$opt]
              or set --append args $backup
            else
              set --append args $backup
            end
          else
            for i in (seq (count $args) 2)
              test -f $args[$i]
              or continue
              reg "File |$backup[$i]| not found"
              set --erase backup[$i]
            end
            string match -qv restore $args
            or return 1
          end
          set -l bookmarks
          for arg in $args[2..-1]
            set --append bookmarks (sed '/^#/ d' $arg \
            | sed -r 's/([^\])\s/\1\n/g')
          end
          $cmd --save $bookmarks

        #Saving a backup into a file
        else
          set -l bookmarks (find $nav_bookmarks -type l 2>/dev/null \
          | string match -r -- "(?<=$nav_bookmarks/).+")
          if test -n "$args[2]"
            nav_instructions "nav -b/--backup"
            return 1
          else if not string length -q $bookmarks
            err "$cmd: There are no bookmarks available to backup"
            return 1
          else if test -z "$args"
            set args "bookmarks.bak"
          else if test -d "$args"
            set args "$args/bookmarks.bak"
          end
          if test -e "$args"
            if test -z "$yes"
              read -n 1 -p "wrn \"File |$args| already exists. Overwrite? [y/n]: \"" | string match -qi y
              or return 1
            end
          end
          command mkdir -p (command dirname $args)
          echo "# Bookmark list generated with nav" > $args
          for bookmark in $bookmarks
            echo $bookmark (readlink "$nav_bookmarks/$bookmark" \
            | string escape -n) >> $args
          end
          win "Bookmarks backup saved at |$args|"
        end
      end

      # Call requested option
      if string match -qr [srma] $flag
        nav_manage $flag
      else if string match -q l $flag
        nav_list
      else
        nav_backup
      end
    end

    function "$cmd"_abbr -V args -V cmd -V yes \
    -d 'Add or remove abbreviations for iteractive use.'

      # Check if abbreviations preexist
      set -l abbr w --where t --to f --foward b --back s --save r --remove m --move a --autoremove l --list
      set -l found
      for i in (seq 1 2 (count $abbr))
        set -l j (math $i+1)
        abbr | string match -qr -- "^$abbr[$i] "
        and set --append found $abbr[$i]
      end

      # Add abbreviations and aliases, or remove them if they preexist
      if test -n "$found"
        if test -z "$yes"
          wrn 'The following abbreviations will be deleted: |'(string join '|, |' $found)'|.'
          read -n 1 -P 'Continue? [y/n]: ' | string match -qi y
          or return 1
        end
        for match in $found
          abbr -e $match
        end
        win 'Abbreviations removed'
      else
        for i in (seq 1 2 (count $abbr))
          abbr -a $abbr[$i] nav $abbr[(math $i + 1)]
        end
        win 'Abbreviations added'
      end
      fish
    end

    # Call requested option
    if string match -qr '[twbf]' $flag
      "$cmd"_navigate $flag $modifier
    else if string match -qr '[srmald]' $flag
      "$cmd"_bookmark $flag
    else if string match -qr x $flag
      "$cmd"_abbr
    else if string match -qr h $flag
      "$cmd"_instructions
      test -z "$args"
    else
      test -n "$args"
      and $cmd --where $args
      or $cmd --list
    end
  end

  # Call main function and unload auxiliary function before finishing
  "$cmd"_main $argv
  set -l exit_status $status
  functions -e (functions | string match -ar "^$cmd.+") dependency
  test $exit_status -eq 0
end
