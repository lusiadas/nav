# Remove abbreviations and global variables
set -l abbr w --where t --to f --foward b --back s --save \
r --remove m --move a --autoremove l --list
for i in (seq 1 2 (count $abbr))
  set -l j (math $i+1)
  abbr | string match -q "$abbr[$i] $package $abbr[$j]"
  and abbr -e $abbr[$i]
end
set --erase nav_bookmarks
set --erase nav_history

# Offer to uninstall dependencies
wget -qO $path/dependency.fish \
https://gitlab.com/lusiadas/dependency/raw/master/dependency.fish
source $path/dependency.fish
and dependency -rp percol -P feedback -P contains_opts grep sed mlocate mc tree
functions -e dependency update_history
