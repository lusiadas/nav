set -l cmd (command basename (status -f) | command cut -f 1 -d '.')
function "$cmd"_completions -V cmd

  # Load dependency
  source (command dirname (status -f))/../dependency.fish
  and dependency -P https://gitlab.com/lusiadas/contains_opts
  or return 1

  # Add options descriptions
  set -l opts t to w where f foward b back p print c commander s save r remove m move l list a autoremove h help d backup x abbr y assume_yes
  complete -fc $cmd -n "not contains_opts (string match -rv -- '^(p|print|c|commander|y|yes)\$' $opts)" \
  -s t -l to -d 'Go to a bookmarked directory'
  complete -fc $cmd -n "not contains_opts (string match -rv -- '^(p|print|c|commander|y|yes)\$' $opts)" \
  -s w -l where -d 'Go to a directory in the navigation history'
  complete -c $cmd -n "not contains_opts (string match -rv -- '^(p|print|c|commander|y|yes)\$' $opts)" \
  -s f -l foward -d 'Go to a child directory'
  complete -fc $cmd -n "not contains_opts (string match -rv -- '^(p|print|c|commander|y|yes)\$' $opts)" \
  -s b -l back -d 'Go to a parent directory'
  complete -c $cmd -n "not contains_opts (string match -rv -- '^(t|to|w|where|f|foward|b|back|y|yes)\$' $opts)" \
  -s p -l print -d 'Print destination instead'
  complete -c $cmd -n "not contains_opts (string match -rv -- '^(t|to|w|where|f|foward|b|back|y|yes)\$' $opts)" \
  -s c -l commander -d 'Open with midnight manager'
  complete -rc $cmd -n "not contains_opts (string match -rv -- '^(y|yes)\$' $opts)" -s s -l save -d \
  'Bookmark directory'
  complete -fc $cmd -n "not contains_opts (string match -rv -- '^(y|yes)\$' $opts)" -s r -l remove -d \
  'Remove bookmark'
  complete -fc $cmd -n "not contains_opts (string match -rv -- '^(y|yes)\$'    $opts)" -s m -l move -d \
  'Move or rename bookmarks and bookmark folders'
  complete -fc $cmd -n 'not contains_opts' -s l -l list -d \
  'List bookmarks'
  complete -fc $cmd -n 'not contains_opts' -s a -l autoremove -d \
  'Remove bookmarks of directories that no longer exist'
  complete -rc $cmd -n "not contains_opts (string match -rv -- '^(y|yes)\$'    $opts)" -s d -l backup -d \
  'Save bookmarks into, or restore them from, a file'
  complete -c $cmd -n 'contains_opts d backup' -a 'restore' -d \
  'Restore bookmarks from a file'
  complete -fc $cmd -n "not contains_opts (string match -rv -- '^(y|yes)\$'    $opts)" -s x -l abbr -d \
  'Add, or remove, abbreviations for iteractive use'
  complete -fc $cmd -n 'not contains_opts' -s h -l help -d \
  'Display instructions'
  complete -fc $cmd -n 'not contains_opts h help a autorenove l list' -s y -l assume_yes -d \
  'Automatically confirm any question'

  # List bookmarks
  set -l bookmarks (command find $nav_bookmarks -type l 2>/dev/null \
  | string match -r "(?<=$nav_bookmarks/).+")
  if test -n "$bookmarks"
    complete -c $cmd -n 'contains_opts r remove' -a 'all' -d \
    'Delete all bookmarks'
    complete -c $cmd -n 'contains_opts r remove' -a 'history' -d \
    'Delete the navigation history'
    for bookmark in $bookmarks
      set -l path (command readlink "$nav_bookmarks/$bookmark")
      complete -fc $cmd -n 'contains_opts t to r remove m move' -a "$bookmark" -d "$path"
      complete -rc $cmd -n 'contains_opts s save' -a "$bookmark" -d "$path"
    end
  end

  # List folders
  set -l folders (find $nav_bookmarks -type d -printf '%P\n' 2>/dev/null)
  if test -n "$folders"
    for folder in $folders
      set -l contents (ls "$nav_bookmarks/$folder")
      complete -fc $cmd -n 'contains_opts r remove m move l list' -a "$folder" -d "$contents"
    end
  end

  # List history entries
  for path in (command sort -u $nav_history 2>/dev/null)
    complete -c $cmd -n 'contains_opts w where' -a "$path"
  end

  # List parent folders
  for i in (command seq (string match -ar / $PWD | wc -l))
    string match -r "([^/]*/){$i}" $PWD | read -l path
    complete -fc $cmd -n 'contains_opts b back' -a "$path"
  end
end

"$cmd"_completions
functions -e "$cmd"_completions dependency
